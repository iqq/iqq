// Handle click on tawkto buttons
$( window ).on( 'load', function () {

    var section     = $( 'div.contact' );
    var form        = $( 'form', section );
    var button      = $( '[data-tawkto]', section );
    var resetButton = $( '[type="reset"]', form );

    if ( undefined !== Tawk_API && "online" === Tawk_API.getStatus() ) {
        console.log('one');
        button.on( 'click', function ( e ) {
            e.preventDefault();
            Tawk_API.toggle();
        } );
    } else {
        console.log('two');
        button.on( 'click', function ( e ) {
            e.preventDefault();
            section.toggleClass( 'active' );
        } );
    }

    resetButton.on( 'click', function ( e ) {
        section.toggleClass( 'active' );
    } );

} );