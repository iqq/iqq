<section class="static-boxes section">

	<div class="wrapper">

		<div class="box video">
			<div class="content" style="background-image:url(images/samples/2.jpg);">
				<a href="#"><span class="sr-only">Spela upp video</span></a>
			</div>
		</div>
		<div class="box text">
			<div class="content">
				<div>
					<h3>Varför fiber?</h3>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<a href="#">Beställ nu</a>
				</div>
			</div>
		</div>
		<div class="box text">
			<div class="content">
				<div>
					<h3>Erbjudande</h3>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<a href="#">Beställ nu</a>
				</div>
			</div>
		</div>
		<div class="box text">
			<div class="content">
				<div>
					<h3>Visste du att?</h3>

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<a href="#">Läs mer</a>
				</div>
			</div>
		</div>
		<div class="box video">
			<div class="content" style="background-image:url(images/samples/3.jpg)">
				<a href="#"><span class="sr-only">Spela upp video</span></a>
			</div>
		</div>

	</div>

	<a href="#">Call to action</a>

</section>