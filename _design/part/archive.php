<div class="section posts-list">
	<div>

		<div class="sidebar">
			<h1 class="section-title">
				<small>Lorem ipsum dolor sit</small>
				Artiklar
			</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur, culpa deserunt dicta doloremque
				eligendi in ipsam ratione repellat sequi voluptatem! Eaque fugiat laboriosam nam quam qui recusandae
				soluta
				suscipit unde.</p>
			<h3><i class="fa fa-tags"></i> Taggar</h3>
			<p>
				<a href="#">Lorem</a>, <a href="#">ipsum</a>, <a href="#">sit</a>, <a href="#">eligendi</a>
			</p>
		</div>

		<div>
			<?php for ( $i = 0; $i < 5; $i ++ ) : ?>
				<article>
					<figure class="featured-image">
						<img src="../images/article-1.png"/>
					</figure>
					<div class="text">
						<header>
							<h2 class="post-title">
								<a href="#">Lorem ipsum dolor sit amet</a>
							</h2>
							<p>
								<strong>Författare: Thor Brink</strong><br>
								<i class="fa fa-tag"></i> <a href="#">CSS</a>, <a href="#">PHP</a>
							</p>
						</header>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dignissimos nobis temporibus.
							A,
							accusamus ad amet fuga laudantium mollitia nobis placeat totam velit? Deleni et fuga
							laudantium mollitia nobis placeat totam velit? Deleniti illum maxime
							nihil
							nobis sequi temporibus...</p>
					</div>
				</article>
			<?php endfor ?>

			<nav>
				<ul class="pagination">
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li>
						<a href="#" aria-label="Next">
							<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
						</a>
					</li>
				</ul>
			</nav>

		</div>
	</div>
</div>