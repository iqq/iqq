<?php $slide_titles = ['El', 'Fiber & Stadsnät', 'Internet', 'TV'] ?>
<section class="slider-section cover">
	<div class="slider">
		<?php for ( $i = 1; $i < 5; $i ++ ) : ?>
			<div class="jumbotron" style="
				background-image:url(images/samples/<?php echo $i ?>.jpg);" data-slide-title="<?php echo $slide_titles[$i-1] ?>">
				<div class="slide-wrapper">
					<div class="content">

						<h2 class="title">Internet Bjärehalvön!</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper augue ac urna aliquet imperdiet. Integer porta, urna eu sagittis consectetur.</p>
						<p><a href="#" role="button">Kontrollera om möjlighet finns</a>

						</p>
					</div>
				</div>
			</div>
		<?php endfor ?>
	</div>
	<div class="colors">
		<div class="color"></div>
		<div class="color"></div>
		<div class="color"></div>
		<div class="color"></div>
	</div>
</section>