<section class="facebook-section">

	<h2>
		<small>Dela & Gilla på</small>
		<i class="fa fa-facebook"></i> Facebook
	</h2>

	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt odio viverra urna congue cursus. Integer
		viverra urna vitae neque mattis suscipit. Suspendisse potenti.</p>

	<div class="wrapper">

		<div class="share">
			<div class="share-buttons">
				<div class="content-tip">
					<a href="https://sv-se.facebook.com/bjarekraft" class="facebook" target="_blank">Dela sidan på
						Facebook</a>

					<div>
						<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/"
						     data-layout="button"
						     data-action="like" data-show-faces="false" data-share="true"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	</div>

</section>