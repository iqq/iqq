<div class="contact-section section">
	<h2>
		<small>Vi finns för dig</small>
		Kontakta oss
	</h2>

	<div>
		<div>
			<div>

				<h3>Kontaktuppgifter</h3>

				<dl class="contact-list">
					<dt>Växel</dt>
					<dd>0431-44 99 00</dd>
					<dt>Felanmälan El</dt>
					<dd>0431-44 99 33</dd>
					<dt>Support Bredband & TV</dt>
					<dd>0771-38 38 38</dd>
					<dt>Fakturafrågor</dt>
					<dd>0431-44 99 00</dd>
					<dt>Elnätsfrågor</dt>
					<dd>kundservice@bjarekraft.se</dd>
					<dt>Bredbandsfrågor</dt>
					<dd>kundservice.bredband@bjarekraft.se</dd>
				</dl>

				<div>
					<div>
						<h3>Besöksadresser</h3>
						<address>
							<p>
								Bjäre Kraft<br>
								Kraftgatan 4<br>
								269 74 Västra Karup
							</p>
						</address>
						<address>
							<p>
								Bjäre Kraft<br>
								Långskeppsgatan 1<br>
								262 71 Ängelholm
							</p>
						</address>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<h3>Postadress</h3>
						<address>
							<p>
								Bjäre Kraft<br>
								Box 1054<br>
								269 21 Båstad
							</p>
						</address>
					</div>
				</div>

			</div>
			<div>
				<h3>Kontaktformulär</h3>
				<form class="contact-form">
					<div class="form-group">
						<label>Vad heter du?</label>
						<input type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Vad gäller ditt ärende?</label>
						<input type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Var kan vi nå dig?</label>
						<input type="text" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Ställ en fråga?</label>
						<textarea class="form-control"></textarea>
					</div>
					<button type="submit">Skicka</button>
				</form>

			</div>
		</div>
	</div>

</div>