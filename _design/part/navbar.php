<nav class="target-group-nav">
	<div class="container-fluid contained">
		<a class="navbar-brand" href="#">
			<span class="sr-only">Bjärekraft</span>
			<img src="images/site-logo.svg"/>
		</a>
		<div class="callout">
			<p><strong>Över 6000</strong><br>nöjda kunder!</p>
		</div>
		<ul class="piped-nav">
			<li class="active">Privatperson</li>
			<li><a href="#">Företag</a></li>
			<li><a href="#">BRF & SAMF</a></li>
			<li class="facebook"><a href="#"><span class="sr-only">Bjärekraft på Facebook</span></a></li>
		</ul>
	</div>
</nav>

<nav class="navbar navbar-default" id="main-navigation">
	<div class="colors">
		<div class="color"></div>
		<div class="color"></div>
		<div class="color"></div>
		<div class="color"></div>
	</div>
	<div class="container-fluid contained">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
			        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				Meny <span class="glyphicon glyphicon-menu-hamburger"></span>
			</button>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

			<ul class="nav navbar-nav">
				<li><a href="#"><span class="glyphicon glyphicon-home"></span><span class="sr-only">Startsida</span></a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Bredband <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="has-image"><a href="#"><span class="fa fa-desktop icon"></span>  Fiber/Stadsnät <span class="description">Lorem ipsum dolor sit amet</span></a></li>
						<li class="has-image"><a href="#"><span class="glyphicon glyphicon-hdd icon"></span>  ADSL <span class="description">Lorem ipsum dolor sit amet</span></a></li>
					</ul>
				</li>
				<li><a href="#">TV</a></li>
				<li><a href="#">El</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="#">Kundservice</a></li>
				<li><a href="#">Om Oss</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-user"></span> Mina Sidor</a></li>
				<li><a href="#"><span class="glyphicon glyphicon-resize-horizontal"></span> Fjärrhjälp</a></li>
				<li class="gradient-1"><a href="#"><span class="glyphicon glyphicon-signal"></span> Driftstatus</a></li>
			</ul>

		</div>
		<!-- /.navbar-collapse -->

	</div>
	<!-- /.container-fluid -->
</nav>