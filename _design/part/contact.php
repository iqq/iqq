<div class="contact">
	<div>
		<div>
			<img src="../images/personal-hans.jpg"/>
			<div>
				<div class="text">
					<h3 class="title">Lorem ipsum dolor</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam, consectetur corporis
						deserunt dolores est explicabo fugiat, harum.</p>
					<p>
						<i class="fa fa-phone"></i> +46 (0)70-848 37 82<br>
						<i class="fa fa-envelope"></i> hans@iqq.se</a>
					</p>
					<a class="button cta" data-tawkto href="#">Kontakta oss</a>
				</div>

				<form class="contact-form">

					<div class="form-group">
						<label for="contact-name">Namn</label>
						<div>
							<input type="text" name="contact-name" id="contact-name" class="form-control"/>
						</div>
					</div>
					<div class="form-group">
						<label for="contact-email">E-post</label>
						<div>
							<input type="email" name="contact-email" id="contact-email" class="form-control"/>
						</div>
					</div>

					<div class="form-group">
						<label for="contact-message">Meddelande</label>
						<div>
							<textarea type="email" name="contact-message" id="contact-message" class="form-control"></textarea>
						</div>
					</div>

					<input class="button" type="reset" value="Avbryt"/>
					<input class="button cta" type="submit" value="Skicka"/>
				</form>

			</div>
		</div>
	</div>
</div>
<a href="#" data-post-id="2132123"></a>