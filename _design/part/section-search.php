<section class="search-section">

	<h2>
		<small>Kan jag få fiber?</small>
		<span class="glyphicon glyphicon-flash"></span> Kolla tillgängligheten på fiber
	</h2>

	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt odio viverra urna congue cursus. Integer
		viverra urna vitae neque mattis suscipit. Suspendisse potenti.</p>

	<div class="wrapper">

		<!--<div class="share">
			<span class="facebook">
				Dela sidan på Facebook
				<div class="share-buttons">
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"
					     data-action="like" data-show-faces="false" data-share="true"></div>
				</div>
			</span>
		</div>-->

		<form class="form-inline">
			<div class="form-group">
				<input type="email" class="form-control" id="inputEmail3" placeholder="Skriv in din adress...">
			</div>
			<button type="submit">Kolla om jag kan få fiber</button>
		</form>

	</div>

</section>