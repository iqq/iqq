<section class="newsletter-section">

	<h2>
		<small>Få senaste nytt</small>
		<i class="fa fa-envelope"></i> Nyhetsbrev
	</h2>

	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt odio viverra urna congue cursus. Integer
		viverra urna vitae neque mattis suscipit. Suspendisse potenti.</p>

	<div class="wrapper">

		<!--<div class="share">
			<span class="facebook">
				Dela sidan på Facebook
				<div class="share-buttons">
					<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button"
					     data-action="like" data-show-faces="false" data-share="true"></div>
				</div>
			</span>
		</div>-->

		<form class="form-inline">
			<div class="form-group">
				<input type="email" class="form-control" id="inputEmail3" placeholder="Email"/>
			</div>
			<button type="submit">Anmäl mig nu</button>
		</form>

	</div>

</section>