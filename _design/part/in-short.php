<div class="section in-short">
	<header>
		<div class="section-title-container">
			<h2 class="section-title">
				<small>Vi levererar</small>
				helhetslösningar
			</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A delectus expedita ipsam quasi. Alias aperiam dolores fugiat iusto maxime provident, sint soluta ut. A, atque consequuntur nemo numquam optio tenetur!</p>
		</div>
	</header>
	<div>
		<div>
			<header>
				<img src="../images/ill-design.svg"/>
				<h2 class="title">Design</h2>
			</header>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dignissimos dolores eius eligendi
				ex explicabo fuga, fugit in modi nemo odit, officiis optio recusandae rem tenetur. Aperiam consequuntur
				obcaecati ullam</p>
		</div>
		<div>
			<header>
				<img src="../images/ill-dev.svg"/>
				<h2 class="title">Development</h2>
			</header>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet cum distinctio dolorem doloremque expedita
				harum hic id, inventore ipsum mollitia nemo nostrum numquam quidem reiciendis rem, sunt tempora,
				voluptates voluptatibus</p>
		</div>
		<div>
			<header>
				<img src="../images/ill-main.svg"/>
				<h2 class="title">Maintenance</h2>
			</header>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aspernatur blanditiis cum ducimus
				eaque enim facere fugit impedit pariatur provident quos repellendus, sunt ut. Amet deleniti laudantium
				magnam non officiis.</p>
		</div>
	</div>
</div>