<div class="project-preview">
	<div class="project">
		<div>
			<div>
				<h2 class="section-title">
					<small>IQQ följer med Finax in i urskogen</small>
					Finax Urkraft
				</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, consequatur, doloremque doloribus
					eaque eius ex fugiat libero magnam mollitia officiis perferendis rem similique, tenetur totam
					veniam.
					Eos explicabo nam voluptatibus.</p>
				<p>Samarbete mellan <strong>IQQ</strong> och <strong>Resulify</strong></p>
				<footer>
					<a class="button" href="#">Gå till hemsidan</a>
				</footer>
			</div>
		</div>
		<div>
			<div class="gallery">
				<img src="../images/urkraft-3.jpg"/>
				<img src="../images/urkraft-4.jpg"/>
				<img src="../images/urkraft.jpg"/>
			</div>
		</div>

	</div>

	<img src="../images/urkraft.jpg"/>
</div>

<div class="project-preview">
	<div class="project">
		<div>
			<div>

				<h2 class="section-title">
					<small>IQQ följer med Finax in i urskogen</small>
					Finax Urkraft
				</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, consequatur, doloremque doloribus
					eaque eius ex fugiat libero magnam mollitia officiis perferendis rem similique, tenetur totam
					veniam.
					Eos explicabo nam voluptatibus.</p>
			</div>
			<footer>
				<a class="button" href="#">Gå till hemsidan</a>
			</footer>
		</div>
		<div>
			<div class="gallery">
				<img src="../images/urkraft-3.jpg"/>
				<img src="../images/urkraft-4.jpg"/>
				<img src="../images/urkraft.jpg"/>
			</div>
		</div>

	</div>

	<img src="../images/project-3.jpg"/>
</div>

<div class="project-preview">
	<div class="project">
		<div>
			<div>
				<p class="tag">Utvalt projekt</p>
				<h2 class="section-title">
					<small>IQQ följer med Finax in i urskogen</small>
					Finax Urkraft
				</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, consequatur, doloremque doloribus
					eaque eius ex fugiat libero magnam mollitia officiis perferendis rem similique, tenetur totam
					veniam.
					Eos explicabo nam voluptatibus.</p>
			</div>
			<footer>
				<a class="button" href="#">Gå till hemsidan</a>
			</footer>
		</div>
		<div>
			<div class="gallery">
				<img src="../images/urkraft-3.jpg"/>
				<img src="../images/urkraft-4.jpg"/>
				<img src="../images/urkraft.jpg"/>
			</div>
		</div>

	</div>

	<img src="../images/project-2.jpg"/>
</div>