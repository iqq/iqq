<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>IQQ</title>

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="images/favicon.png"/>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/app.css">

	<script>try {
			Typekit.load( { async: true } );
		} catch ( e ) {
		}</script>

<body>

<nav class="navbar navbar-default" id="main-navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
			        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				Meny
				<i class="fa fa-ellipsis-v"></i>
			</button>
			<a class="navbar-brand" href="#"><img src="images/logo-header.png"/></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar">
				<li class="active"><a href="#">Hem <span class="sr-only">(current)</span></a></li>
				<li><a href="#">Projekt</a></li>
				<li><a href="#">Vision</a></li>
				<li><a href="#"><span class="fa fa-wordpress fa"></span> WordPress</a></li>
				<li class="cta"><a href="#">Kontakta oss</a></li>
			</ul>
			<ul class="nav navbar-nav navbar social-nav">
				<li class="icon-only facebook"><a href="#"><i class="fa fa-facebook"></i><span class="sr-only">Facebook</span></a></li>
				<li class="icon-only twitter"><a href="#"><i class="fa fa-twitter"></i><span class="sr-only">Twitter</span></a></li>
				<li class="icon-only instagram"><a href="#"><i class="fa fa-instagram"></i><span class="sr-only">Instagram</span></a></li>
				<li class="icon-only linkedin"><a href="#"><i class="fa fa-linkedin"></i><span class="sr-only">LinkedIn</span></a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>