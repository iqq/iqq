<div class="social">
	<h2 class="section-title">
		<small>Lorem ipsum dolor sit amet</small>
		Sociala kanaler
	</h2>
	<div>
		<div>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-instagram"></i></a>
		</div>
	</div>
</div>
<footer id="site-footer">
	<div>
		<div>
			<p>Tel: 0708 48 37 82, E-post: <a href="#">info@iqq.se</a>, Regementsgatan 8, 211 34 Malmö | Stadsgården 6, 104 65 Stockholm</p>
		</div>
	</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/app.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/56e2b7561e82531d56937acd/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>