<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// Include local configuration
if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
	include( dirname( __FILE__ ) . '/local-config.php' );
}

// Global DB config
if ( ! defined( 'DB_NAME' ) ) {
	define( 'DB_NAME', 'iqq' );
}
if ( ! defined( 'DB_USER' ) ) {
	define( 'DB_USER', 'root' );
}
if ( ! defined( 'DB_PASSWORD' ) ) {
	define( 'DB_PASSWORD', 'root' );
}
if ( ! defined( 'DB_HOST' ) ) {
	define( 'DB_HOST', 'localhost' );
}

/** Database Charset to use in creating database tables. */
if ( ! defined( 'DB_CHARSET' ) ) {
	define( 'DB_CHARSET', 'utf8' );
}

/** The Database Collate type. Don't change this if in doubt. */
if ( ! defined( 'DB_COLLATE' ) ) {
	define( 'DB_COLLATE', '' );
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', 'B[J7Q1]MrYwG##h)?&cyS RJ<aA7F*q3~Y4&N@F^3J,n:rLH(_W9(ixE.MEL-]%Q' );
define( 'SECURE_AUTH_KEY', 'uj2{~bll)/iwW~<Y$Svde;,E{(G-)ApM+:k4@qCxuRDgXne-Es+*Si%Oenf+X;jL' );
define( 'LOGGED_IN_KEY', 't};[^S8}T$0`ocr)zE~i5?/QyaPW:B<FG{n Q9:|JrK8`CW+74&<U_V({e:_ov.:' );
define( 'NONCE_KEY', '/4^2uq}(&ZFa6((n m<&xt/shU+Hdt]oa,fAGs+x6!);@5A<6%e?_{v?,$zUV@0/' );
define( 'AUTH_SALT', 'caU9S]_+_9`mAAeVr${%}3zxv/IqMcu>:d.>N2Tcfc)[9ontjf3Ibe|.zK[}n&^v' );
define( 'SECURE_AUTH_SALT', 'T|G {GAL-qQsiM-QBe?{4++CSXB$,%jCTG$+7+L>[8PmBc1(Q J],>d_am}?WpRA' );
define( 'LOGGED_IN_SALT', '4aENdW|bjJWluSl{-?mH7xGl ((^d@4C,mPM#@*w)/6H^F;3u||}uZtviLsCSM`w' );
define( 'NONCE_SALT', '-ASX@XXs-@(!xfq#A1ORT`:GWi# xR(rsD~4I w2!MSrTquw!NH?pvDbZM9~d{$/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'iqq_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define( 'WPLANG', '' );


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if ( ! defined( 'WP_SITEURL' ) ) {
	define( 'WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/core' );
}
if ( ! defined( 'WP_HOME' ) ) {
	define( 'WP_HOME', 'http://' . $_SERVER['SERVER_NAME'] . '' );
}
if ( ! defined( 'WP_CONTENT_DIR' ) ) {
	define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
}
if ( ! defined( 'WP_CONTENT_URL' ) ) {
	define( 'WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/content' );
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}

if ( ! defined( 'WP_DEFAULT_THEME' ) ) {
	define( 'WP_DEFAULT_THEME', 'iqq' );
}


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
