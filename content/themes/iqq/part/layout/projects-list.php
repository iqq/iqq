<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$posts_per_page     = get_sub_field( 'posts-per-page' );
$featured_projects  = get_sub_field( 'featured' );
$more_projects_page = get_sub_field( 'more-projects-link' );

if ( ! empty( $more_projects_page ) ) {
	$more_projects_url = get_the_permalink( $more_projects_page->ID );
}

$args = array(
	'post_type' => 'project',
);

if ( empty( $posts_per_page ) ) {
	$args['posts_per_page'] = - 1;
} else {
	$args['posts_per_page'] = $posts_per_page;
}

if ( ! empty( $featured_projects ) ) {

	$featured_projects_ids = array_map( function ( $project ) {
		return $project->ID;
	}, $featured_projects );

	$args['post__in'] = $featured_projects_ids;

}

$projects = get_posts( $args );
?>


<?php if ( ! empty( $projects ) ) : ?>

	<div class="project-preview">

		<?php foreach ( $projects as $project ) : ?>

			<?php
			$title_small      = get_field( 'title-small', $project->ID );
			$title            = get_field( 'title', $project->ID );
			$content          = get_field( 'content', $project->ID );
			$button_text      = get_field( 'button-text', $project->ID );
			$button_url       = get_field( 'button-url', $project->ID );
			$gallery          = get_field( 'gallery', $project->ID );
			$background_image = get_field( 'background-image', $project->ID );
			?>

			<div class="project">
				<div>
					<div>

						<?php if ( in_array( $project->ID, $featured_projects_ids ) ) : ?>
							<p class="tag"><?php _e( 'Utvalt projekt', 'iqq' ) ?></p>
						<?php endif ?>

						<?php if ( ! empty( $title ) || ! empty( $title_small ) ) : ?>
							<h2 class="section-title">

								<?php if ( ! empty( $title_small ) ) : ?>
									<small><?php echo $title_small ?></small>
								<?php endif ?>

								<?php if ( ! empty( $title ) ) : ?>
									<?php echo $title ?>
								<?php endif ?>

							</h2>
						<?php endif ?>

						<?php if ( ! empty( $content ) ) : ?>
							<?php echo $content ?>
						<?php endif ?>

						<footer>

							<?php if ( ! empty( $button_text ) && ! empty( $button_url ) ) : ?>
								<a class="button" target="_blank" href="<?php echo esc_url( $button_url ) ?>"><?php echo wp_kses( $button_text ) ?></a>
							<?php endif ?>

							<?php if ( ! empty( $more_projects_url ) ) : ?>
								<a class="button cta" href="<?php echo esc_url( $more_projects_url ) ?>"><?php _e( 'Visa fler projekt', 'iqq' ) ?></a>
							<?php endif ?>

						</footer>

					</div>
				</div>
				<div>
					<div class="gallery">
						<?php if ( ! empty( $gallery ) ) : ?>
							<?php foreach ( $gallery as $image ) : ?>
								<?php echo wp_get_attachment_image( $image['ID'], array( 1920, 0 ) ) ?>
							<?php endforeach ?>
						<?php endif ?>
					</div>
				</div>

			</div>

			<?php if ( ! empty( $background_image ) ) : ?>
				<div class="parallax">
					<?php echo wp_get_attachment_image( $background_image['ID'], array( 1920, 0 ) ) ?>
				</div>
			<?php endif ?>

		<?php endforeach; ?>

	</div>

<?php endif ?>