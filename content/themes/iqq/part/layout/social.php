<?php
if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly

$title_small = get_sub_field( 'title-small' );
$title       = get_sub_field( 'title' );
$facebook    = get_field( 'facebook-page-url', 'options' );
$twitter     = get_field( 'twitter-page-url', 'options' );
$linkedin    = get_field( 'linkedin-page-url', 'options' );
$instagram   = get_field( 'instagram-page-url', 'options' );
?>


<div class="social">

	<?php if ( ! empty( $title ) || ! empty( $title_small ) ) : ?>
		<h2 class="section-title">
			<?php if ( ! empty( $title_small ) ) : ?>
				<small><?php echo $title_small ?></small>
			<?php endif ?>
			<?php if ( ! empty( $title ) ) : ?>
				<?php echo $title ?>
			<?php endif ?>
		</h2>
	<?php endif ?>

	<div>
		<div>

			<?php if ( ! empty( $facebook ) ) : ?>
				<a target="_blank" href="<?php echo esc_url( $facebook ) ?>">
					<i class="fa fa-facebook"></i>
					<span class="sr-only">Facebook</span>
				</a>
			<?php endif ?>

			<?php if ( ! empty( $twitter ) ) : ?>
				<a target="_blank" href="<?php echo esc_url( $twitter ) ?>">
					<i class="fa fa-twitter"></i>
					<span class="sr-only">Twitter</span>
				</a>
			<?php endif ?>

			<?php if ( ! empty( $linkedin ) ) : ?>
				<a target="_blank" href="<?php echo esc_url( $linkedin ) ?>">
					<i class="fa fa-linkedin"></i>
					<span class="sr-only">LinkedIn</span>
				</a>
			<?php endif ?>

			<?php if ( ! empty( $instagram ) ) : ?>
				<a target="_blank" href="<?php echo esc_url( $instagram ) ?>">
					<i class="fa fa-instagram"></i>
					<span class="sr-only">Instagram</span>
				</a>
			<?php endif ?>

		</div>
	</div>
</div>