<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$title       = get_sub_field( 'title' );
$title_small = get_sub_field( 'title-small' );
$text        = get_sub_field( 'text' );
?>

<?php if ( have_rows( 'content' ) ) : ?>
	<div class="section teasers">
		<?php if ( ! empty( $title ) || ! empty( $title_small ) || ! empty( $text ) ) : ?>
			<header>
				<div class="section-title-container">

					<?php if ( ! empty( $title ) || ! empty( $title_small ) ) : ?>
						<h2 class="section-title">
							<?php if ( ! empty( $title_small ) ) : ?>
								<small><?php echo sanitize_text_field( $title_small ) ?></small>
							<?php endif ?>
							<?php if ( ! empty( $title ) ) : ?>
								<?php echo sanitize_text_field( $title ) ?>
							<?php endif ?>
						</h2>
					<?php endif ?>

					<?php if ( ! empty( $text ) ) : ?>
						<p><?php echo sanitize_text_field( $text ) ?></p>
					<?php endif ?>
				</div>
			</header>
		<?php endif ?>

		<div>
			<?php while ( have_rows( 'content' ) ) : the_row(); ?>
				<div>
					<?php if ( get_sub_field( 'title' ) ) : ?>
						<header>
							<?php if ( get_sub_field( 'image' ) ) : ?>

								<?php
								$image         = get_sub_field( 'image' );
								$image_element = wp_get_attachment_image( $image['ID'], array( 20, 0 ) );
								echo $image_element;
								?>

							<?php endif ?>
							<h2 class="title"><?php the_sub_field( 'title' ) ?></h2>
						</header>
					<?php endif ?>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur dignissimos dolores
						eius
						eligendi
						ex explicabo fuga, fugit in modi nemo odit, officiis optio recusandae rem tenetur. Aperiam
						consequuntur
						obcaecati ullam</p>
				</div>
			<?php endwhile ?>
		</div>

	</div>
<?php endif ?>