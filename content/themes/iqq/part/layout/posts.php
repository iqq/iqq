<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$title           = get_sub_field( 'title' );
$posts_per_page  = get_sub_field( 'posts-per-page' );
$link_to_archive = get_sub_field( 'link-to-archive' );
$category        = get_sub_field( 'category' );
$args            = array(
	'posts_per_page' => ( empty( $posts_per_page ) ) ? get_option( 'posts_per_page' ) : $posts_per_page
);

if ( ! empty( $category ) ) {
	$args['cat'] = $category;
}

$posts_query = new WP_Query( $args );

?>

<?php if ( $posts_query->have_posts() ) : ?>
	<div class="posts section">
		<div>
			<?php if ( ! empty( $title ) ) : ?>
				<h2 class="section-title"><?php echo $title ?></h2>
			<?php endif ?>
			<div>
				<?php while ( $posts_query->have_posts() ) : ?>
					<?php $posts_query->the_post() ?>
					<article>
						<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<a href="<?php the_permalink() ?>">
									<?php the_post_thumbnail() ?>
								</a>
							</figure>
						<?php endif ?>
						<div class="content">
							<header>
								<h3>
									<a href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
								</h3>
								<p class="date"><?php echo get_the_date() ?> - <?php the_category( ',' ) ?></p>
							</header>
							<?php the_excerpt() ?>
						</div>
					</article>
				<?php endwhile ?>
			</div>
			<?php if ( ! empty( $link_to_archive ) && ! empty( get_option( 'page_for_posts' ) ) ) : ?>
				<footer class="section-footer">
					<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>"
					   class="section-button"><?php _e( 'More news', 'iqq' ) ?></a>
				</footer>
			<?php endif ?>
		</div>
	</div>
<?php endif ?>
<?php wp_reset_query() ?>
