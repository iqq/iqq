<?php
if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly

$title       = get_sub_field( 'image' );
$content     = get_sub_field( 'content' );
$button_text = get_sub_field( 'button-text' );
$image       = get_sub_field( 'image' );
?>

<?php if ( ! empty( $title ) || ! empty( $content ) ) : ?>
	<div class="contact">
		<div>
			<div>
				<?php if ( ! empty( $image ) ) : ?>
					<?php echo wp_get_attachment_image( $image['ID'], array( 100, 100 ) ) ?>
				<?php endif ?>
				<div>

					<?php if ( ! empty( $content ) ) : ?>
						<div class="text">
							<?php echo apply_filters( 'the_content', $content ) ?>
							<?php if ( ! empty( $button_text ) ) : ?>
								<a class="button cta" data-tawkto href="#"><?php the_sub_field( 'button-text' ) ?></a>
							<?php endif ?>
						</div>
					<?php endif ?>

					<?php if ( class_exists( 'GFAPI' ) && ( ! empty( $contact_form_id = get_option( 'iqq-theme-contact-form-id' ) ) ) ) : ?>
						<?php echo do_shortcode( '[gravityform id=' . esc_attr( $contact_form_id ) . ' title=false description=false ajax=true tabindex=49]' ) ?>
					<?php endif ?>

				</div>
			</div>
		</div>
	</div>
	<a href="#" data-post-id="2132123"></a>
<?php endif ?>