<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$content = get_sub_field( 'content' );
?>

<?php if ( ! empty( $content ) ) : ?>
	<footer class="site-footer">
		<div>
			<div>
				<?php echo $content ?>
			</div>
		</div>
	</footer>
<?php endif ?>