<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

$image = get_sub_field( 'image' );
$title = get_sub_field( 'title' );
$text  = get_sub_field( 'text' );
?>
<div class="hero">
	<div>
		<div>
			<?php if ( ! empty( $title ) ) : ?>
				<h1><?php echo sanitize_text_field( $title ) ?></h1>
			<?php endif ?>
			<?php if ( ! empty( $text ) ) : ?>
				<p><?php echo sanitize_text_field( $text ) ?></p>
			<?php endif ?>
		</div>
	</div>
	<?php if ( ! empty( $image ) ) : ?>
		<?php print_r( wp_get_attachment_image( $image['ID'], array( 1900, 0 ) ) ) ?>
	<?php endif ?>
</div>