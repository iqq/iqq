<div class="content">
	<div>
		<h2 class="section-title">About Eklund Foundation</h2>
		<blockquote>
			We are very pleased to establish this foundation in line with the vision of good oral health for
			everyone, which is central to us. For five decades, TePe has worked in collaboration with universities,
			institutes and dental care professionals around the world. The foundation is a way for us to show our
			appreciation and create something that will contribute to knowledge and development within the
			odontological field for many years to come.
			<footer>
				<cite>Joel Eklund, CEO, TePe Munhygienprodukter AB</cite>
			</footer>
		</blockquote>
		<p>
			The Eklund Foundation is based on a long-standing relationship with the dental community and
			dedication to preventive dental care. In 1965, Henning Eklund founded TePe, an enterprise sparked
			by the innovation of the triangular wooden dental stick, designed together with the Malmö School of
			Dentistry.
		</p>
		<p>
			Henning Eklund’s son, Bertil Eklund, developed the business through international expansion, and
			Joel Eklund, third generation Eklund and current CEO, carries the heritage forward. Continuous
			product development with research findings as the starting point has resulted in a wide high quality
			range, created with the vision of making good oral health possible for everyone.
		</p>
		<p>
			The idea of recognizing and stimulating odontological science had been present for a long time with
			the Eklund family, and in 2015, it took practical form. In connection with TePe’s 50th anniversary, Bertil
			Eklund announced the establishment of Eklund Foundation for Odontological Research and
			Education, based on the donation of 5 330 779 EUR / 50m SEK, thus securing many years of future
			delivery.
		</p>
		<p>
			The foundation will primarily distribute funds for research and education that encourage knowledge
			development within the oral field. In 2016, applications are accepted through the online application
			portal from 1 May, and the successful candidates will be announced in autumn 2016.
		</p>
	</div>
</div>