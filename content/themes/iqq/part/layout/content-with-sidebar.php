<?php
if ( ! defined( 'ABSPATH' ) )
	exit; // Exit if accessed directly

$title_small     = get_sub_field( 'title-small' );
$title           = get_sub_field( 'title' );
$content         = get_sub_field( 'content' );
$sidebar_content = get_sub_field( 'sidebar-content' );
?>

<div class="content">
	<div>
		<div class="editor-content">

			<?php if ( ! empty( $title ) || ! empty( $title_small ) ) : ?>
				<h1 class="section-title">
					<?php if ( ! empty( $title_small ) ) : ?>
						<small><?php echo $title_small ?></small>
					<?php endif ?>
					<?php if ( ! empty( $title ) ) : ?>
						<?php echo $title ?>
					<?php endif ?>
				</h1>
			<?php endif ?>

			<?php if ( ! empty( $content ) ) : ?>
				<?php echo $content ?>
			<?php endif ?>

		</div>

		<?php if ( ! empty( $sidebar_content ) ) : ?>
			<div class="sidebar">
				<?php echo $sidebar_content ?>
			</div>
		<?php endif ?>

	</div>
</div>