<?php
global $section_counter;
$section_counter = 1;

require( dirname( __FILE__ ) . '/inc/wp_bootstrap_navwalker.php' );

class theme {

	/**
	 * Setting up hooks and actions
	 */

	static function init() {
		add_action( 'init', array( __CLASS__, 'register_menus' ) );
		add_action( 'acf/init', array( __CLASS__, 'add_options_pages' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'include_advanced_custom_fields' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'theme_support' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'image_sizes' ) );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'set_up_scripts' ) );
		add_action( 'after_setup_theme', array( __CLASS__, 'load_theme_textdomain' ) );
		add_filter( 'sanitize_file_name', array( __CLASS__, 'sanitize_file_name' ), 10, 2 );
		add_action( 'init', array( __CLASS__, 'create_gf_contact_form' ) );
		add_action( 'init', array( __CLASS__, 'register_post_type_project' ) );
		add_shortcode( 'fontawesome', array( __CLASS__, 'shortcode_fontawesome' ) );

	}

	/**
	 * Add option page
	 */
	static function add_options_pages() {

		if ( function_exists( 'acf_add_options_page' ) ) :

			acf_add_options_sub_page( array(
				'page_title'  => __( 'Footer', 'iqq' ),
				'menu_title'  => __( 'Footer', 'iqq' ),
				'menu_slug'   => 'theme-footer-settings',
				'parent_slug' => 'themes.php',
			) );

			acf_add_options_sub_page( array(
				'page_title'  => __( 'Sociala Medier', 'iqq' ),
				'menu_title'  => __( 'Sociala Medier', 'iqq' ),
				'menu_slug'   => 'social-media-settings',
				'parent_slug' => 'themes.php',
			) );

		endif;

	}

	static function get_social_stream() {
		require( trailingslashit( __DIR__ ) . 'inc/facebook.php' );
		exit;
	}

	/**
	 * Remove unwanted characters from filenames on upload
	 *
	 * @param $filename
	 *
	 * @return string
	 */
	static function sanitize_file_name( $filename ) {
		$sanitized_filename = remove_accents( $filename );

		return $sanitized_filename;
	}

	static function set_up_scripts() {

		wp_enqueue_style( 'iqq-theme', esc_url( get_stylesheet_directory_uri() . "/css/app.css" ) );

		wp_enqueue_script( 'iqq-theme', esc_url( get_stylesheet_directory_uri() ) . "/js/app.js", array(
			'jquery',
			'underscore',
		), true, true );
	}


	static function theme_support() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
	}

	/**
	 * Add image sizes
	 */
	static function image_sizes() {
		add_image_size( 'gallery', 277, 277, true );
	}

	static function register_menus() {
		register_nav_menus(
			array(
				'primary' => __( 'Huvudmeny', 'iqq' )
			)
		);
	}

	static function load_theme_textdomain() {

		load_theme_textdomain( 'iqq', get_template_directory() . '/languages' );
	}

	/**
	 * Load theme libraries
	 */
	static function include_advanced_custom_fields() {

		// If Advanced Custom Fields is not already available.
		if ( ! class_exists( 'acf' ) ) {

			add_filter( 'acf/settings/path', function ( $path ) {
				// update path
				$path = get_stylesheet_directory() . '/lib/acf/';

				return $path;
			} );

			add_filter( 'acf/settings/dir', function ( $dir ) {
				// update path
				$dir = get_stylesheet_directory_uri() . '/lib/acf/';

				return $dir;
			} );

			include( __DIR__ . '/lib/acf/acf.php' );
		}
	}

	static function create_gf_contact_form() {

		if ( ! class_exists( 'GFAPI' ) )
			return;

		$form_title = __( 'Kontakt', 'iqq' );

		if ( empty( $form_id = get_option( 'iqq-theme-contact-form-id' ) ) ) {
			$form_id = GFAPI::add_form( array(
				'title' => $form_title
			) );

			if ( ! is_wp_error( $form_id ) ) {
				update_option( 'iqq-theme-contact-form-id', $form_id );
			}
		}

		// Create form fields
		$field_name = new GF_Field_Text( array(
			'label'      => __( 'Namn', 'iqq' ),
			'id'         => 1,
			'isRequired' => true,
			'cssClass'   => 'contact-name'
		) );

		$field_email = new GF_Field_Email( array(
			'label'      => __( 'E-post', 'iqq' ),
			'id'         => 2,
			'isRequired' => true,
			'cssClass'   => 'contact-email'
		) );

		$field_message = new GF_Field_Textarea( array(
			'label'      => __( 'Meddelande', 'iqq' ),
			'id'         => 3,
			'isRequired' => true,
			'cssClass'   => 'contact-message'
		) );

		GFAPI::update_form( array(
			'is_active' => true,
			'title'     => $form_title,
			'cssClass'  => 'contact-form',
			'fields'    => array(
				$field_name,
				$field_email,
				$field_message
			),
			'button'    => array(
				'text' => __( 'Skicka', 'iqq' ),
			)
		), $form_id );
	}

	// Register Custom Post Type
	static function register_post_type_project() {

		$labels = array(
			'name'                  => _x( 'Projekt', 'Post Type General Name', 'iqq' ),
			'singular_name'         => _x( 'Projekt', 'Post Type Singular Name', 'iqq' ),
			'menu_name'             => __( 'Projekt', 'iqq' ),
			'name_admin_bar'        => __( 'Projekt', 'iqq' ),
			'archives'              => __( 'Projektarkiv', 'iqq' ),
			'parent_item_colon'     => __( '', 'iqq' ),
			'all_items'             => __( 'Alla Projekt', 'iqq' ),
			'add_new_item'          => __( 'Lägg till nytt Projekt', 'iqq' ),
			'add_new'               => __( 'Lägg till nytt', 'iqq' ),
			'new_item'              => __( 'Nytt Projekt', 'iqq' ),
			'edit_item'             => __( 'Redigera Projekt', 'iqq' ),
			'update_item'           => __( 'Uppdatera Projekt', 'iqq' ),
			'view_item'             => __( 'Visa Projekt', 'iqq' ),
			'search_items'          => __( 'Sök Projekt', 'iqq' ),
			'not_found'             => __( 'Hittades ej', 'iqq' ),
			'not_found_in_trash'    => __( 'Hittades ej i papperskorgen', 'iqq' ),
			'featured_image'        => __( 'Utvald bild', 'iqq' ),
			'set_featured_image'    => __( 'Välj utvald bild', 'iqq' ),
			'remove_featured_image' => __( 'Ta bort utvald bild', 'iqq' ),
			'use_featured_image'    => __( 'Använd som utvald bild', 'iqq' ),
			'insert_into_item'      => __( 'Lägg till', 'iqq' ),
			'uploaded_to_this_item' => __( 'Uppladdat till detta Projekt', 'iqq' ),
			'items_list'            => __( 'Projektlista', 'iqq' ),
			'items_list_navigation' => __( 'Projektnavigation', 'iqq' ),
			'filter_items_list'     => __( 'Filtrera Projekt', 'iqq' ),
		);
		$args   = array(
			'label'               => __( 'Projekt', 'iqq' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-welcome-view-site',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( 'project', $args );

	}

	static function shortcode_fontawesome( $atts ) {

		$atts = shortcode_atts( array(
			'icon' => 'fa-bug'
		), $atts, 'fontawesome' );

		return '<i class="fa fa-' . esc_attr( $atts['icon'] ) . '"></i>';
	}
}

theme::init();