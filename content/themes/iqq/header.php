<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php get_template_part( 'part/script-header' ) ?>
</head>
<body <?php body_class() ?>>

<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
			        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				Menu <i class="icon icon-bars"></i>
			</button>
			<a class="navbar-brand" href="<?php echo esc_url( get_bloginfo( 'url' ) ) ?>">
				<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/images/logo-header.png' ) ?>"/>
			</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php
			wp_nav_menu( array(
					'menu'           => 'primary',
					'theme_location' => 'primary',
					'depth'          => 1,
					'container'      => false,
					'menu_class'     => 'nav navbar-nav navbar',
					'fallback_cb'    => false,
					'walker'         => new wp_bootstrap_navwalker()
				)
			);
			?>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>