<?php get_header() ?>

<?php if ( have_posts() ) : ?>
	<div class="posts section">
		<div>
			<h2 class="section-title"><?php _e( 'News', 'iqq' ) ?></h2>
			<div>
				<?php while ( have_posts() ) : ?>
					<?php the_post() ?>
					<article>
						<?php if ( has_post_thumbnail() ) : ?>
							<figure>
								<a href="<?php the_permalink() ?>">
									<?php the_post_thumbnail() ?>
								</a>
							</figure>
						<?php endif ?>
						<div class="content">
							<header>
								<h3>
									<a href="<?php the_permalink() ?>"><?php echo the_title() ?></a>
								</h3>
								<p class="date"><?php echo get_the_date() ?> - <?php the_category( ',' ) ?></p>
							</header>
							<?php the_excerpt() ?>
						</div>
					</article>
				<?php endwhile ?>
			</div>
		</div>
	</div>
<?php endif ?>

<?php get_footer() ?>
