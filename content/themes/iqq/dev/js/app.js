@import "../bower_components/modernizr/modernizr.js";
@import "../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js";
@import "../bower_components/jquery.syncHeight/jquery.syncHeight.min.js";
@import "../bower_components/slick-carousel/slick/slick.js";
@import "../bower_components/Materialize/js/parallax.js";
@import "../bower_components/Materialize/js/velocity.min.js";
@import "../bower_components/Materialize/js/materialbox.js";

+function () {

    var $ = jQuery;

    /** Breakpoints **/
    @import "part/_breakpoints.js";

    /** Jumbotron **/
    @import "part/_jumbotron.js";

    /** Jumbotron **/
    @import "part/_posters.js";

    @import "part/_tawkto.js";

    $( document ).ready( function () {
        $( '.parallax' ).parallax();
    } );

    $( document ).ready( function () {
        $( '.project .gallery img' ).materialbox();
    } );

}();


