// Handle click on tawkto buttons
$( window ).on( 'load', function () {

    var section     = $( 'div.contact' );
    var form        = $( 'form', section );
    var button      = $( '[data-tawkto]', section );
    var resetButton = $( '[type="reset"]', form );

    if ( undefined !== window.Tawk_API && "online" === window.Tawk_API.getStatus() ) {
        console.log( 'one' );
        button.on( 'click', function ( e ) {
            e.preventDefault();
            window.Tawk_API.toggle();
        } );
    } else {
        console.log( 'two' );
        button.on( 'click', function ( e ) {
            e.preventDefault();
            section.toggleClass( 'active' );
        } );
    }

    resetButton.on( 'click', function ( e ) {
        section.toggleClass( 'active' );
    } );


    (function () {
        var contactSection = $( 'div.contact' );

        contactSection.each( function () {
            var wrapper     = $( this ).find( '> div > div > div' );
            var contactForm = $( '.contact-form-wrapper', section );
            var textSection = $( 'div.text', section );

            wrapper.css('min-height', function() {
                return (contactForm.height() > textSection.height()) ? contactForm.height() + 'px' : textSection.height() + 'px';
            });
        } );
    })();


} );