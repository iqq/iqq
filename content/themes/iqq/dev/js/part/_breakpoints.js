/** Breakpoints **/
var breakpointElements = '<div class="bp-xs visible-xs"></div><div class="bp-sm visible-sm"></div><div class="bp-md visible-md"></div><div class="bp-lg visible-lg"></div>';
$('body').append(breakpointElements);

var breakPoints = {
    sm : 768,
    md : 992,
    lg : 1200
};

function isBreakpoint(bp) {
    return ($('.bp-'+bp).is(':visible'));
};