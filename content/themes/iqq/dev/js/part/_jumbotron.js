/** jumbotron.js **/

$.fn.setJumbotronAspectRatio = function () {

    return this.each( function () {

        var $jumbotron = $( this );

        if ( '' !== $jumbotron.css( 'background-image' ) ) {
            // If Jumbotron has background image.

            var image    = $jumbotron.css( 'background-image' );
            var imageSrc = image.replace( /url\((.+)+\)/, '$1' ).replace( /"/g, '' );

            var imageResource = new Image();
            imageResource.src = imageSrc;

            var width  = imageResource.width;
            var height = imageResource.height;

            $( window ).on( 'load resize', _.debounce( function () {

                var newHeight = height / width * $jumbotron.width()

                $jumbotron.css( 'height', newHeight );

            }, 300 ) );
        }

        $( window ).trigger( 'resize' );

    } );

};

// Initialize
+function () {

    $( window ).on( 'load', function () {
        $( '.jumbotron' ).setJumbotronAspectRatio();
    } );


}();