module.exports = function ( grunt ) {

    // load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
    require('load-grunt-tasks')(grunt);
    require( 'time-grunt' )( grunt );

    // Project configuration.
    grunt.initConfig( {
        pkg: grunt.file.readJSON( 'package.json' ),
        sass: {
            options: {
                sourceMap: true,
                includePaths: [ 'bower_components/' ],
                outputStyle: 'compressed'
            },
            dev: {
                files: {
                    '../css/app.css': 'scss/app.scss'
                }
            },
            dist: {
                options: {
                    sourceMap: false
                },
                files: {
                    '../css/app.css': 'scss/app.scss'
                }
            }
        },
        import: {
            options: {},
            dev: {
                src: 'js/app.js',
                dest: '../js/app.js'
            }
        },
        watch: {
            styles: {
                files: 'scss/**/*.scss',
                tasks: [ 'sass:dev' ],
                options: {
                    interrupt: true,
                    livereload: true
                }
            },
            js: {
                files: 'js/**/*.js',
                tasks: [ 'import:dev' ],
                options: {
                    interrupt: true
                }
            }
        }
    } );

    // Default task(s).
    grunt.registerTask( 'default', [ 'sass:dev', 'import', 'watch' ] );
    grunt.registerTask( 'dist', [ 'sass:dist', 'import' ] );

};