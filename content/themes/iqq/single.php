<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly ?>
<?php get_header() ?>

<?php if ( have_posts() ) : ?>

	<?php while ( have_posts() ) : the_post() ?>

		<div class="content section">
			<div>
				<h1 class="section-title post-title"><?php the_title() ?></h1>
				<div>

					<article>
						<div class="content">
							<header>
								<p class="date"><?php echo get_the_date() ?> - <?php the_category( ',' ) ?></p>
							</header>
							<?php the_content() ?>
						</div>
					</article>

				</div>
			</div>
		</div>

	<?php endwhile ?>

<?php endif ?>

<?php get_footer() ?>